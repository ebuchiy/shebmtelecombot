package webm

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"tgbot/config"
	"time"
)

/*
"-v", "error",
            "-threads", str(FFMPEG_THREADS),
            "-i", "pipe:0", # read input from stdin
            "-fs", "19999850", ##limit filesize
            "-map", "V:0?", # select video stream
            "-map", "0:a?", # ignore audio if doesn't exist
            "-c:v", "libx264", # specify video encoder
            "-max_muxing_queue_size", "9999", # https://trac.ffmpeg.org/ticket/6375
            "-movflags", "+faststart", # optimize for streaming
            "-preset", "slow", # https://trac.ffmpeg.org/wiki/Encode/H.264#a2.Chooseapresetandtune
            "-timelimit", "900", # prevent DoS (exit after 15 min)
            "-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",
*/

func convertWebm(filepath string) (*[]byte, error) {
	outputfile := fmt.Sprintf("%v.mp4", time.Now().Unix())
	outputFilepath := fmt.Sprint(config.Config.Webm.SaveFolder, outputfile)
	// ffmpeg -threads 5 -hwaccel vaapi -vaapi_device /dev/dri/renderD128 -hwaccel_output_format vaapi -y -f webm -i 1555136761.webm -fs 19999850 -map "V:0?" -map "0:a?" -c:v h264_vaapi -max_muxing_queue_size 9999 -movflags +faststart -preset slow -timelimit 900 -vf pad="ceil(iw/2)*2:ceil(ih/2)*2" -f mp4 hui.mp4

	/*	ffmpeg := exec.Command(
		"ffmpeg",
		"-y",
		"-c:v libx264",
		"-crf 25",
		"-profile:v high",
		"-level 4.2",
		"-max_muxing_queue_size 4096",
		"-pix_fmt yuv420p",
		"-preset medium",
		"-threads", strconv.Itoa(config.Config.Webm.Threads),
		"-map V:0?",
		"-map 0:a?",
		"-timelimit 900",
		"-movflags +faststart",
		"-strict -2",
		"-vf scale=trunc(iw/2)*2:trunc(ih/2)*2",
		"-i", filepath,
		outputFilepath)
	log.Println(ffmpeg.Args)*/
	ffmpeg := exec.Command(
		"ffmpeg",
		"-threads", strconv.Itoa(config.Config.Webm.Threads),
		"-y",
		"-f", "webm",
		"-i", filepath,
		"-fs", "19999850",
		"-map", "V:0?",
		"-map", "0:a?",
		"-c:v", "libx264",
		"-max_muxing_queue_size", "9999",
		"-movflags", "+faststart",
		"-preset", "fast", // slow

		"-crf", "28",
		"-profile:v", "high",
		"-level", "4.2",
		"-max_muxing_queue_size", "4096",
		"-movflags", "+faststart",
		"-strict", "-2",
		"-pix_fmt", "yuv420p",

		"-timelimit", "900",
		"-vf", "pad=ceil(iw/2)*2:ceil(ih/2)*2",
		"-f", "mp4",
		outputFilepath)

	ffmpeg.Stdout = os.Stdout
	ffmpeg.Stderr = os.Stdout

	if err := ffmpeg.Run(); err != nil {
		return nil, errors.Wrap(err, "\nffmpeg.Start()")
	}

	bytes, err := ioutil.ReadFile(outputFilepath)
	if err != nil {
		return nil, errors.Wrap(err, "\nioutil.ReadFile()")
	}

	os.Remove(outputFilepath)

	return &bytes, nil
}

func downloadFile(url string, filepath string) error {
	response, err := http.Head(url)
	if err != nil {
		return err
	}
	fileSize, _ := strconv.Atoi(response.Header.Get("Content-Length"))
	if fileSize > 51380224 {
		return errors.New("file too big")
	}
	response, err = http.Get(url)
	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()
	if err != nil {
		return err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	if http.DetectContentType(bytes) != "video/webm" {
		return errors.New("not webm")
	}
	ioutil.WriteFile(filepath, bytes, 0644)
	return nil
}

func processMP4(upd tgbotapi.Update, api *tgbotapi.BotAPI, link string) error {
	outputfile := fmt.Sprintf("%v.webm", time.Now().Unix())
	outputfilepath := fmt.Sprint(config.Config.Webm.SaveFolder, outputfile)
	err := downloadFile(link, outputfilepath)
	if err != nil {
		return errors.Wrap(err, "\nfailed to download")
	}

	mess := tgbotapi.NewMessage(upd.Message.Chat.ID, "Конверчу")
	mess.ReplyToMessageID = upd.Message.MessageID
	sent_mess, _ := api.Send(mess)

	output, err := convertWebm(outputfilepath)
	if err != nil {
		return errors.Wrap(err, "\nfailed to convert")
	}

	video := tgbotapi.NewVideoUpload(upd.Message.Chat.ID, tgbotapi.FileBytes{
		Name:  "video.mp4",
		Bytes: *output,
	})
	video.ReplyToMessageID = upd.Message.MessageID
	_, _ = api.Send(video)

	api.DeleteMessage(tgbotapi.NewDeleteMessage(sent_mess.Chat.ID, sent_mess.MessageID))
	os.Remove(outputfilepath)

	return nil
}

func WebmUrlHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	regexp, err := regexp.Compile(`(http.?://.*\.(?:webm))`)
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "regexp compilation error")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "regexp compilation error")))
		api.Send(mess)
		return
	}

	url := regexp.Find([]byte(upd.Message.Text))

	err = processMP4(upd, api, string(url))
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to process")))
		api.Send(mess)
		return
	}

}

func DocumentHandler(upd tgbotapi.Update, api *tgbotapi.BotAPI) {
	if !strings.HasSuffix(upd.Message.Document.FileName, ".webm") {
		log.Println("Received not webm. Skipping...")
		return
	}

	file, err := api.GetFile(tgbotapi.FileConfig{FileID: upd.Message.Document.FileID})
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to get file")))
		api.Send(tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to get file"))))
		return
	}

	link := file.Link(api.Token)

	err = processMP4(upd, api, link)
	if err != nil {
		log.Println(fmt.Sprintf("Error: %+v", errors.Wrap(err, "failed to process")))
		mess := tgbotapi.NewMessage(upd.Message.Chat.ID, fmt.Sprintf("Error: %v", errors.Wrap(err, "failed to process")))
		api.Send(mess)
		return
	}
}
