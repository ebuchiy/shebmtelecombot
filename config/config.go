package config

import (
	"github.com/pelletier/go-toml"
	"io/ioutil"
	"log"
)

type Configuration struct {
	Bot struct {
		Apikey        string `toml:"apikey"`
		Debug         bool   `toml:"debug"`
		Developer     []int  `toml:"developer"`
		DeveloperName string `toml:"developer_name"`
	} `toml:"Bot"`

	Logs struct {
		SaveLogs bool   `toml:"saveLogs"`
		LogFile  string `toml:"logFile"`
	} `toml:"Logs"`

	Proxy struct {
		Enable bool   `toml:"enable"`
		IP     string `toml:"ip"`
		Port   string `toml:"port"`
	} `toml:"Proxy"`

	Webm struct {
		SaveFolder string `toml:"save_folder"`
		Threads    int    `toml:"threads"`
	} `toml:"Webm"`
}

func (c *Configuration) Load(path string) {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln(err)
	}
	err = toml.Unmarshal(dat, c)
	if err != nil {
		log.Fatalln("!", err)
	}
}

var Config Configuration
